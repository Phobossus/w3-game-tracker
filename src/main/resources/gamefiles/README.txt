FAQ
1. How to add new games for W3-GameTracker to load?
* open Games.txt
* each provider (ENT, MMH, GHP) has game names under its name
* go to one of these sites:
https://entgaming.net/forum/games.php - for ENT games
https://ghostplay.de/stats/ - for GhostPlay games
http://www.makemehost.com/games.php - for MakeMeHost games
* find a bot hosted game and copy its name

IMPORTANT: You can only add games which look like this
name of the game #number
i.e [ENT] Footmen Frenzy 5.4 #21

You must NOT copy the #number part, so for the example above, appropriate game name is "[ENT] Footmen Frenzy" without "#21"

* add the copied name between the provider's name and the "-----" sign

This illustrates properly added Footmen Frenzy game to the ENT provider.
ENT
[ENT] Castle Fight 2v2
[ENT] Legion TD
[ENT] Footmen Frenzy 5.4
-----

* to remove a game, simply delete its name from the file

2. I want to customize the text copied when "Get Modes" button is clicked.
* For each game, you can specify your custom mode setting that you want to be copied with "Get Modes"
as well as auto-copied when the game you've joined becomes full.
To do that, simply add <<-your -mode -setting>> after game name in Games.txt

Example:
ENT
[ENT] Castle Fight 2v2<<-rr2 -na -ntb -ban1 -bal1 -ur -gld30 -skip>>
[ENT] Castle Fight 1v1<<-rr2 -mp2 -slb -rban2 -skip>>
[ENT] Footmen Frenzy 5.4

Warning: Pay attention to spaces - there must be NO space between the game name and the "<<" sign, so this:
[BK] Castle Fight <<-rr2 -na -ntb>>
won't work (I might add less strict rules in the future)

3. Games.txt has incorrect content. How to fix it?
Read this document carefully :)


