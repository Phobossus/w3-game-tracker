package notifiers;

import controls.UserInterface;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class FullGameNotifier extends Observer implements Notifier {

    private String soundFileName;
    private boolean minimizeOnFull;
    private int minimizeDelay;
    private boolean muteOnFull;
    private int timeUntilCopyGameAgain;
    private String userName;
    private UserInterface userInterface;

    public FullGameNotifier(String soundFileName, boolean minimizeOnFull, int minimizeDelay, boolean muteOnFull,
                            int timeUntilCopyGameAgain, String userName, UserInterface userInterface) {
        this.soundFileName = soundFileName;
        this.minimizeOnFull = minimizeOnFull;
        this.minimizeDelay = minimizeDelay;
        this.muteOnFull = muteOnFull;
        this.timeUntilCopyGameAgain = timeUntilCopyGameAgain;
        this.userName = userName;
        this.userInterface = userInterface;
    }

    @Override
    public void onUpdate(List<String> players) {
        boolean userInGame = isInGame(userName, players);
        if (soundFileName != null) {
            if (userInGame) {
                userInterface.disableAutocopyForTime(timeUntilCopyGameAgain);
                userInterface.forceGetModes();
            }
            soundPlayer.playSound(soundFileName);
        }
        if (minimizeOnFull && userInGame) {
            ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
            executorService.schedule(new Runnable() {
                                         @Override
                                         public void run() {
                                             userInterface.minimize();
                                         }
                                     }
                    , minimizeDelay, TimeUnit.SECONDS);
        }
        if (muteOnFull && userInGame) {
            userInterface.sound(false);
        }
    }

    private boolean isInGame(String userName, List<String> players) {
        for (String player : players) {
            if (userName.equalsIgnoreCase(player)) {
                return true;
            }
        }
        return false;
    }
}
