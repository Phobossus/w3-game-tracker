package notifiers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class SoundPlayer {

    private static final Logger LOGGER = LogManager.getLogger(SoundPlayer.class);
    private Map<String, Clip> clips = new HashMap<>();
    private boolean muted;

    public void playSound(String soundFileName) {
        if (!muted) {
            try {
                Clip clip;
                if (clips.containsKey(soundFileName)) {
                    clip = clips.get(soundFileName);
                    clip.setFramePosition(0);
                } else {
                    File file = Paths.get("notifications/" + soundFileName).toFile();
                    if (file.exists()) {
                        AudioInputStream audioIn = AudioSystem.getAudioInputStream(file);
                        clip = AudioSystem.getClip();
                        clip.open(audioIn);
                        clips.put(soundFileName, clip);
                    } else {
                        throw new IOException("Sound file name not found.");
                    }
                }
                clip.start();
            } catch (IOException | UnsupportedAudioFileException | LineUnavailableException e) {
                LOGGER.error("Error while attempting to play the sound file", e);
            }
        }
    }

    public void mute() {
        muted = true;
    }

    public void unmute() {
        muted = false;
    }
}
