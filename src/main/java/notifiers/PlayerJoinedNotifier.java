package notifiers;

import controls.UserInterface;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PlayerJoinedNotifier extends Observer implements Notifier {

    private static final Logger LOGGER = LogManager.getLogger(PlayerJoinedNotifier.class);
    private String soundFileName;
    private String userName;
    private Map<String, Boolean> observedPlayersNotificationsSent;
    private UserInterface userInterface;
    private boolean active;

    public PlayerJoinedNotifier(String soundFileName, String userName, List<String> observedPlayers, UserInterface userInterface) {
        this.soundFileName = soundFileName;
        this.userName = userName;
        this.observedPlayersNotificationsSent = toMap(observedPlayers);
        if (soundFileName != null) {
            active = true;
        }
        this.userInterface = userInterface;
    }

    @Override
    public void onUpdate(List<String> players) {
        if (active) {
            if (isInGameLobby(userName, players)) {
                userInterface.sound(true);
            }
            updatePlayersNotificationsSent(players);
            for (String playerInGame : players) {
                if (observedPlayersNotificationsSent.containsKey(playerInGame)) {
                    if (!observedPlayersNotificationsSent.get(playerInGame)) {
                        observedPlayersNotificationsSent.put(playerInGame, true);
                        LOGGER.info("Observed player " + playerInGame + " has joined.");
                        soundPlayer.playSound(soundFileName);
                    }
                }
            }
        }
    }

    private void updatePlayersNotificationsSent(List<String> players) {
        for (String playerNotifSent : observedPlayersNotificationsSent.keySet()) {
            if (observedPlayersNotificationsSent.get(playerNotifSent)) {
                if (!isInGameLobby(playerNotifSent, players)) {
                    observedPlayersNotificationsSent.put(playerNotifSent, false);
                }
            }
        }
    }

    private boolean isInGameLobby(String player, List<String> players) {
        for (String playerInGame : players) {
            if (playerInGame.equalsIgnoreCase(player)) {
                return true;
            }
        }
        return false;
    }

    private Map<String, Boolean> toMap(List<String> observedPlayers) {
        Map<String, Boolean> observedPlayersNotificationsSent = new HashMap<>();
        for (String player : observedPlayers) {
            observedPlayersNotificationsSent.put(player, false);
        }
        return observedPlayersNotificationsSent;
    }
}
