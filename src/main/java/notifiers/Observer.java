package notifiers;

import java.util.List;

public abstract class Observer {

    public void onUpdate(List<String> players) {
        throw new UnsupportedOperationException();
    }

}
