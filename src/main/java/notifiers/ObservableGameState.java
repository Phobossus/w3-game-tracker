package notifiers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class ObservableGameState {

    private ExecutorService executorService = Executors.newFixedThreadPool(2);
    private List<Observer> observers = new ArrayList<>();

    public void registerNotifiers(List<Notifier> notifiers) {
        for (Notifier n : notifiers) {
            observers.add((Observer) n);
        }
    }

    public Optional<Observer> getSpecificObserver(Class c) {
        for (Observer o : observers) {
            if (c.isInstance(o)) {
                return Optional.of(o);
            }
        }
        return Optional.empty();
    }

    public List<Observer> getRegisteredNotifiers() {
        return observers;
    }

    public ExecutorService getExecutorService() {
        return executorService;
    }

}
