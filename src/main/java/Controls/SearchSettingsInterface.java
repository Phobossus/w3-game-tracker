package controls;

import tracker.providers.ProviderType;
import games.GameSearchSettings;
import games.GameSearchSettingsController;
import games.GamesCollection;
import gui.SearchSettingsWindowUI;

import javax.swing.*;
import java.util.List;
import java.util.Map;

// When Adds are programmed, after clicking OK in search settings, Games.txt should be updated
public class SearchSettingsInterface {

    private final UserInterface userInterface;
    private final SearchSettingsWindowUI searchSettingsWindowUI;
    private final JFrame searchSettingsFrame;
    private final GameSearchSettingsController gameSearchSettingsController;
    private GameSearchSettings currentGameSearchSettings;

    public SearchSettingsInterface(UserInterface userInterface, SearchSettingsWindowUI searchSettingsWindowUI,
                                   GameSearchSettings defaultGameSearchSettings) {
        this.userInterface = userInterface;
        this.searchSettingsWindowUI = searchSettingsWindowUI;
        this.searchSettingsFrame = new JFrame("SearchSettingsWindowUI");
        this.gameSearchSettingsController = new GameSearchSettingsController(searchSettingsFrame);
        this.currentGameSearchSettings = defaultGameSearchSettings;
    }

    public void open(GamesCollection gamesCollection) {
        searchSettingsFrame.setTitle("Game search settings");
        searchSettingsFrame.setContentPane(searchSettingsWindowUI.getGamePanel());
        searchSettingsFrame.setLocationRelativeTo(null);
        searchSettingsFrame.setResizable(false);
        searchSettingsFrame.pack();
        searchSettingsFrame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        searchSettingsFrame.setAlwaysOnTop(true);
        displayGameList(gamesCollection);
    }

    public void showSettingsInterface(boolean show) {
        searchSettingsFrame.setVisible(show);
    }

    public synchronized void updateGameSearchSettings(GameSearchSettings gameSearchSettings) {
        this.currentGameSearchSettings = gameSearchSettings;
    }

    public synchronized String getGameToSearch(ProviderType providerType) {
        return currentGameSearchSettings.getGameName(providerType);
    }

    public GameSearchSettingsController getGameSearchSettingsController() {
        return gameSearchSettingsController;
    }

    private void displayGameList(GamesCollection gamesCollection) {
        Map<ProviderType, List<String>> gameMap = gamesCollection.getGamesMap();
        for (ProviderType providerType : ProviderType.values()) {
            DefaultListModel<String> listModel = new DefaultListModel<>();
            JList<String> jList = searchSettingsWindowUI.getJList(providerType);
            for (String gameName : gameMap.get(providerType)) {
                listModel.addElement(gameName);
            }
            jList.setModel(listModel);
            if (jList.isSelectionEmpty()) {
                jList.setSelectedValue(currentGameSearchSettings.getGameName(providerType), true);
            }
        }

    }

}
