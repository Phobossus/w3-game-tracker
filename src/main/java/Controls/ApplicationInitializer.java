package controls;

import tracker.providers.GameStateProvider;
import tracker.providers.ProviderType;
import games.GameSearchSettings;
import games.GamesCollection;
import notifiers.FullGameNotifier;
import notifiers.Notifier;
import notifiers.PlayerJoinedNotifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import utils.ErrorDialogs;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class ApplicationInitializer {

    private static final Logger LOGGER = LogManager.getLogger(ApplicationInitializer.class);
    private final ApplicationSettings applicationSettings;
    private final UserInterface userInterface;
    private final List<Notifier> registeredNotifiers;
    private final GamesCollection gamesCollection;
    private final GameSearchSettings defaultGameSearchSettings;
    private final Map<ProviderType, GameStateProvider> gameStateProviders;

    public ApplicationInitializer() {
        this.applicationSettings = new ApplicationSettings("config.properties");
        this.userInterface = new UserInterface(this);
        this.registeredNotifiers = new LinkedList<>();
        this.gamesCollection = loadGamesCollection();
        this.defaultGameSearchSettings = loadDefaultGameSearchSettings();
        this.gameStateProviders = loadGameStateProviders();
    }

    public void initialize() {
        registerNotifiers();
        userInterface.open();
    }

    public GameSearchSettings getDefaultGameSearchSettings() {
        return defaultGameSearchSettings;
    }

    public Map<ProviderType, GameStateProvider> getGameStateProviders() {
        return gameStateProviders;
    }

    public List<Notifier> getRegisteredNotifiers() {
        return registeredNotifiers;
    }

    public ApplicationSettings getApplicationSettings() {
        return applicationSettings;
    }

    public GamesCollection getGamesCollection() {
        return gamesCollection;
    }

    private Map<ProviderType, GameStateProvider> loadGameStateProviders() {
        Map<ProviderType, GameStateProvider> gameStateProviders = new EnumMap<>(ProviderType.class);
        for (ProviderType providerType : ProviderType.values()) {
            gameStateProviders.put(providerType, GameStateProvider.createGameStateProvider(providerType,
                    defaultGameSearchSettings.getGameName(providerType),
                    gamesCollection));
        }
        return gameStateProviders;
    }

    private void registerNotifiers() {
        registeredNotifiers.addAll(Arrays.asList(
                new FullGameNotifier(applicationSettings.getFullGameSound(),
                        applicationSettings.isMinimizeOnFull(),
                        applicationSettings.getMinimizeDelay(),
                        applicationSettings.isMuteOnFull(),
                        applicationSettings.getTimeUntilCopyGameAgain(),
                        applicationSettings.getUsername(),
                        userInterface),
                new PlayerJoinedNotifier(applicationSettings.getObservedPlayerJoinedSound(),
                        applicationSettings.getUsername(),
                        getObservedPlayers(),
                        userInterface)));
    }

    private GamesCollection loadGamesCollection() {
        GamesCollection gamesCollection = new GamesCollection("Games.txt");
        gamesCollection.readGamesFromFile();
        return gamesCollection;
    }

    private GameSearchSettings loadDefaultGameSearchSettings() {
        GameSearchSettings gameSearchSettings = null;
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("search-settings/default"));
            gameSearchSettings = GameSearchSettings.ofProperties(properties);
        } catch (IOException e) {
            ErrorDialogs.showWarningDialog("File 'default' not found! Check your " +
                    "/search-settings/ folder. Right now we will assume you want to play 2v2 Castle Fight.");
        }
        return gameSearchSettings == null ? GameSearchSettings.castleFight() : gameSearchSettings;
    }


    private List<String> getObservedPlayers() {
        List<String> observedPlayers;
        Path path = Paths.get("Observed.txt");
        try {
            observedPlayers = Files.readAllLines(path);
        } catch (IOException e) {
            observedPlayers = Collections.emptyList();
            LOGGER.warn("Failed to locate Observed.txt");
        }
        return observedPlayers;
    }
}
