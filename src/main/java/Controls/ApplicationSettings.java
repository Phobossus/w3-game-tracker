package controls;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import tracker.providers.ProviderType;
import utils.ErrorDialogs;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ApplicationSettings {

    private static final Logger LOGGER = LogManager.getLogger(ApplicationSettings.class);
    private String username;
    private String fullGameSound;
    private String observedPlayerJoinedSound;
    private boolean muteOnFull;
    private boolean minimizeOnFull;
    private int minimizeDelay;
    private int timeUntilCopyGameAgain;
    private ProviderType defaultGameProvider;

    public ApplicationSettings(String configurationFilePath) {
        Properties properties = new Properties();
        try (FileInputStream fileInputStream = new FileInputStream(configurationFilePath)) {
            properties.load(fileInputStream);
            username = properties.getProperty("username");
            fullGameSound = properties.getProperty("fullGameSound");
            observedPlayerJoinedSound = properties.getProperty("observedPlayerJoinedSound");
            muteOnFull = Boolean.parseBoolean(properties.getProperty("muteOnFull"));
            minimizeOnFull = Boolean.parseBoolean(properties.getProperty("minimizeOnFull"));
            minimizeDelay = Integer.parseInt(properties.getProperty("minimizeDelay"));
            timeUntilCopyGameAgain = Integer.parseInt(properties.getProperty("timeUntilCopyGameAgain"));
            defaultGameProvider = ProviderType.valueOf(properties.getProperty("defaultGameProvider"));
        } catch (IOException e) {
            String errorMessage = "Config.properties is missing!";
            ErrorDialogs.showErrorDialog(errorMessage);
            LOGGER.error(errorMessage, e);
            System.exit(1);
        } catch (IllegalArgumentException e) {
            ErrorDialogs.showWarningDialog("Some properties in Config.properties have incorrect values. Verify" +
                    " that to avoid unwanted application behaviour.");
            LOGGER.warn("Problem initializing application settings", e);
            validateMandatoryFields();
        }
    }

    public String getUsername() {
        return username;
    }

    public String getFullGameSound() {
        return fullGameSound;
    }

    public String getObservedPlayerJoinedSound() {
        return observedPlayerJoinedSound;
    }

    public boolean isMuteOnFull() {
        return muteOnFull;
    }

    public boolean isMinimizeOnFull() {
        return minimizeOnFull;
    }

    public int getMinimizeDelay() {
        return minimizeDelay;
    }

    public int getTimeUntilCopyGameAgain() {
        return timeUntilCopyGameAgain;
    }

    public ProviderType getDefaultGameProvider() {
        return defaultGameProvider;
    }

    private void validateMandatoryFields() {
        if (username == null) {
            ErrorDialogs.showErrorDialog("Setting username is mandatory! Check your Config.properties!");
            LOGGER.error("Username was not set.");
            System.exit(1);
        }
        if (defaultGameProvider == null) {
            defaultGameProvider = ProviderType.ENT;
        }
    }
}
