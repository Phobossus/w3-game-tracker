package controls;

import tracker.state.GameState;
import tracker.providers.FeedDownloader;
import tracker.providers.ProviderType;
import gui.GameWindowUI;
import gui.SearchSettingsWindowUI;
import utils.DocumentService;

import javax.swing.*;
import java.awt.*;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class UserInterface {

    private ApplicationInitializer applicationInitializer;
    private SearchSettingsInterface searchSettingsInterface;
    private GameWindowUI gameWindowUI;
    private ProviderType selectedProviderType;

    public UserInterface(ApplicationInitializer applicationInitializer) {
        this.applicationInitializer = applicationInitializer;
    }

    public void open() {
        if (gameWindowUI == null) {
            gameWindowUI = new GameWindowUI(this);
            JFrame frame = new JFrame("GameWindowUI");
            frame.setTitle("W3 Game Tracker");
            frame.setContentPane(gameWindowUI.getPanel());
            frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            frame.setLocationRelativeTo(null);
            frame.setAlwaysOnTop(true);
            frame.setResizable(false);
            frame.pack();
            frame.setVisible(true);
            gameWindowUI.setMainFrame(frame);
            ApplicationSettings applicationSettings = applicationInitializer.getApplicationSettings();
            setDefaultGameProvider(applicationSettings.getDefaultGameProvider());
            SearchSettingsWindowUI searchSettingsWindowUI = new SearchSettingsWindowUI();
            searchSettingsInterface = new SearchSettingsInterface(this, searchSettingsWindowUI,
                    applicationInitializer.getDefaultGameSearchSettings());
            searchSettingsWindowUI.setSearchSettingsInterface(searchSettingsInterface);
            searchSettingsInterface.open(applicationInitializer.getGamesCollection());
            enableSearching();
        }
    }

    public void openSearchSettingsInterface() {
        searchSettingsInterface.showSettingsInterface(true);
    }

    public void enableSearching() {
        FeedDownloader feedDownloader = new FeedDownloader(this, searchSettingsInterface,
                applicationInitializer.getRegisteredNotifiers(), applicationInitializer.getGameStateProviders(),
                new DocumentService());
        new Thread(feedDownloader).start();
        setLoadingMessage(true);
    }

    public void displayGame(GameState gameState) {
        gameWindowUI.getName().setText(gameState.getName());
        gameWindowUI.getSlots().setText(gameState.getSlots());
        gameWindowUI.setModes(gameState.getModes());
        List<String> players = gameState.getPlayers();
        List<JLabel> playersLabels = gameWindowUI.getPlayersLabels();
        Iterator<String> playersIterator = players.iterator();
        Iterator<JLabel> playerLabelsIterator = playersLabels.iterator();
        while (playersIterator.hasNext()) {
            String playerName = playersIterator.next();
            JLabel playerLabel = playerLabelsIterator.next();
            playerLabel.setText(playerName);
        }
        while (playerLabelsIterator.hasNext()) {
            playerLabelsIterator.next().setText("-");
        }
    }

    public void sound(boolean on) {
        JCheckBox muteCheckBox = gameWindowUI.getMute();
        if ((on && muteCheckBox.isSelected()) || (!on && !muteCheckBox.isSelected())) {
            muteCheckBox.doClick();
        }
    }

    public void disableAutocopyForTime(int seconds) {
        gameWindowUI.getAlwaysCopyCheckBox().doClick();
        Executors.newSingleThreadScheduledExecutor().schedule(new Runnable() {
            @Override
            public void run() {
                JCheckBox checkbox = gameWindowUI.getAlwaysCopyCheckBox();
                if (!checkbox.isSelected()) {
                    checkbox.doClick();
                }
            }
        }, seconds, TimeUnit.SECONDS);
    }

    public void forceGetModes() {
        gameWindowUI.getGetModesButton().doClick();
    }

    public void minimize() {
        gameWindowUI.getMainFrame().setState(Frame.ICONIFIED);
    }

    public boolean shouldKeepSearching() {
        return gameWindowUI.getKeepSearchingCheckBox().isSelected();
    }

    public void setLoadingMessage(boolean on) {
        String message = on ? "Loading games..." : "Ready to copy.";
        gameWindowUI.getLoadingText().setText(message);
    }

    public ProviderType getSelectedProviderType() {
        return selectedProviderType;
    }

    public void setSelectedProviderType(ProviderType providerType) {
        this.selectedProviderType = providerType;
    }

    private void setDefaultGameProvider(ProviderType defaultGameProvider) {
        gameWindowUI.getProviderList().setSelectedItem(defaultGameProvider.toString());
    }

}
