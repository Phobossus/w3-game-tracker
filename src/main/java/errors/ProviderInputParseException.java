package errors;

public class ProviderInputParseException extends Exception {

    public ProviderInputParseException(Exception e) {
        super(e);
    }
}
