package games;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import tracker.providers.ProviderType;
import utils.ErrorDialogs;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class GamesCollection {

    private static final Logger LOGGER = LogManager.getLogger(GamesCollection.class);
    private static final String END_OF_LIST = "-----";
    private final Map<ProviderType, List<String>> gamesMap;
    private final Map<String, String> gameModesMap;
    private final String gamesFilePath;

    public GamesCollection(String gamesFilePath) {
        this.gamesMap = new EnumMap<>(ProviderType.class);
        this.gameModesMap = new HashMap<>();
        this.gamesFilePath = gamesFilePath;
    }

    public void readGamesFromFile() {
        Path gamesFile = Paths.get(gamesFilePath);
        try {
            populateGameLists(Files.readAllLines(gamesFile));
        } catch (IOException e) {
            ErrorDialogs.showErrorDialog("Games.txt is missing!");
            System.exit(1);
        }
    }

    public Map<ProviderType, List<String>> getGamesMap() {
        return gamesMap;
    }

    public Map<String, String> getGameModesMap() {
        return gameModesMap;
    }

    private void populateGameLists(List<String> gamesFileLines) {
        for (ProviderType providerType : ProviderType.values()) {
            gamesMap.put(providerType, new LinkedList<>());
        }
        try {
            Iterator<String> gameFileLinesIterator = gamesFileLines.iterator();
            while (gameFileLinesIterator.hasNext()) {
                ProviderType providerType = ProviderType.valueOf(gameFileLinesIterator.next());
                String gameName = gameFileLinesIterator.next();
                while (!gameName.equals(END_OF_LIST)) {
                    String[] nameWithModes = gameName.split("<<");
                    String mode = "You've not specified any modes to copy for " + nameWithModes[0] + ". Check your Games.txt.";
                    if (nameWithModes.length > 1) {
                        mode = nameWithModes[1].split(">>")[0];
                    }
                    gameName = nameWithModes[0];
                    gameModesMap.put(gameName, mode);
                    gamesMap.get(providerType).add(gameName);
                    gameName = gameFileLinesIterator.next();
                }
            }
        } catch (IllegalArgumentException e) {
            LOGGER.error("Error while loading the game list from Games.txt", e);
            ErrorDialogs.showErrorDialog("Games.txt has incorrect content. Check README file to learn more.");
            System.exit(1);
        }
    }
}

