package games;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import utils.ErrorDialogs;

import javax.swing.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.Set;

public class GameSearchSettingsController {

    private static final Logger LOGGER = LogManager.getLogger(GamesCollection.class);
    private final JFileChooser fileChooser = new JFileChooser("search-settings");
    private final JFrame searchSettingsFrame;

    public GameSearchSettingsController(JFrame searchSettingsFrame) {
        this.searchSettingsFrame = searchSettingsFrame;
    }

    public boolean saveDefaultGameSearchSettings(GameSearchSettings gameSearchSettings) {
        boolean success = false;
        Properties properties = GameSearchSettings.toProperties(gameSearchSettings);
        try (FileOutputStream fileOutputStream = new FileOutputStream(Paths.get("search-settings/default").toFile())) {
            properties.store(fileOutputStream, null);
            success = true;
        } catch (IOException e) {
            LOGGER.error("Error saving default settings.", e);
            ErrorDialogs.showErrorDialog("An error occurred while saving this setting as default.");
        }
        return success;
    }

    public void saveGameSearchSettings(GameSearchSettings gameSearchSettings) {
        if (fileChooser.showSaveDialog(searchSettingsFrame) == JFileChooser.APPROVE_OPTION) {
            Properties properties = GameSearchSettings.toProperties(gameSearchSettings);
            try (FileOutputStream fileOutputStream = new FileOutputStream(fileChooser.getSelectedFile())) {
                properties.store(fileOutputStream, null);
            } catch (IOException e) {
                LOGGER.error("Error saving search settings.", e);
                ErrorDialogs.showErrorDialog("Saving your settings has failed");
            }
        }
    }

    public GameSearchSettings loadGameSearchSettings() {
        GameSearchSettings gameSearchSettings = null;
        if (fileChooser.showOpenDialog(searchSettingsFrame) == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            try (FileInputStream fileInputStream = new FileInputStream(file)) {
                Properties properties;
                properties = new Properties();
                properties.load(fileInputStream);
                if (!validate(properties)) {
                    throw new IOException();
                }
                gameSearchSettings = GameSearchSettings.ofProperties(properties);
            } catch (IOException e) {
                LOGGER.error("Error loading game search settings.", e);
                ErrorDialogs.showErrorDialog("There were problems loading this file. (corrupted content?)");
            }
        }
        return gameSearchSettings;
    }

    private boolean validate(Properties properties) {
        Set<String> keys = properties.stringPropertyNames();
        return keys.contains("defEnt") && keys.contains("defMmh") && keys.contains("defGhp");
    }
}
