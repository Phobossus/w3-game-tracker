package games;

import tracker.providers.ProviderType;

import java.util.Properties;

public class GameSearchSettings {
    private String entGame;
    private String mmhGame;
    private String ghpGame;

    public void setEntGame(String entGame) {
        this.entGame = entGame;
    }

    public void setMmhGame(String mmhGame) {
        this.mmhGame = mmhGame;
    }

    public void setGhpGame(String ghpGame) {
        this.ghpGame = ghpGame;
    }

    public String getEntGame() {
        return entGame;
    }

    public String getMmhGame() {
        return mmhGame;
    }

    public String getGhpGame() {
        return ghpGame;
    }

    public static Properties toProperties(GameSearchSettings gameSearchSettings) {
        Properties properties = new Properties();
        properties.setProperty("defEnt", gameSearchSettings.getEntGame());
        properties.setProperty("defMmh", gameSearchSettings.getMmhGame());
        properties.setProperty("defGhp", gameSearchSettings.getGhpGame());
        return properties;
    }

    public static GameSearchSettings ofProperties(Properties properties) {
        GameSearchSettings gameSearchSettings = new GameSearchSettings();
        gameSearchSettings.setEntGame(properties.getProperty("defEnt"));
        gameSearchSettings.setMmhGame(properties.getProperty("defMmh"));
        gameSearchSettings.setGhpGame(properties.getProperty("defGhp"));
        return gameSearchSettings;
    }

    public static GameSearchSettings castleFight() {
        GameSearchSettings gameSearchSettings = new GameSearchSettings();
        gameSearchSettings.setEntGame("[ENT] Castle Fight 2v2");
        gameSearchSettings.setMmhGame("[SN] Castle Fight");
        gameSearchSettings.setGhpGame("[BK] CASTLE FIGHT");
        return gameSearchSettings;
    }

    public String getGameName(ProviderType providerType) {
        switch (providerType) {
            case ENT:
                return entGame;
            case MMH:
                return mmhGame;
            case GHP:
                return ghpGame;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
