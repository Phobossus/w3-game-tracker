package gui;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import controls.SearchSettingsInterface;
import tracker.providers.ProviderType;
import games.GameSearchSettings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class SearchSettingsWindowUI {
    private SearchSettingsInterface searchSettingsInterface;
    private JPanel panel1;
    private JList<String> mmhList;
    private JList<String> ghpList;
    private JButton saveDefaultSearchSettingButton;
    private JButton loadSettingButton;
    private JButton OKButton;
    private JPanel GamePanel;
    private JList<String> entList;
    private JButton addButton;
    private JButton addButton1;
    private JButton addButton2;
    private JButton setAsDefaultButton;
    private JTextField successTextField;
    private JTextField textField2;

    public void setSearchSettingsInterface(SearchSettingsInterface searchSettingsInterface) {
        this.searchSettingsInterface = searchSettingsInterface;
    }

    private void createUIComponents() {

    }

    public JPanel getGamePanel() {
        return GamePanel;
    }

    public SearchSettingsWindowUI() {
        saveDefaultSearchSettingButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                searchSettingsInterface
                        .getGameSearchSettingsController().saveGameSearchSettings(getSelectedGameSearchSettings());
            }
        });
        loadSettingButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Optional<GameSearchSettings> optionalGameSearchSettings =
                        Optional.ofNullable(searchSettingsInterface
                                .getGameSearchSettingsController().loadGameSearchSettings());
                if (optionalGameSearchSettings.isPresent()) {
                    GameSearchSettings gameSearchSettings = optionalGameSearchSettings.get();
                    for (ProviderType providerType : ProviderType.values()) {
                        getJList(providerType).setSelectedValue(gameSearchSettings.getGameName(providerType), true);
                    }
                }
            }
        });
        OKButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                searchSettingsInterface.updateGameSearchSettings(getSelectedGameSearchSettings());
                searchSettingsInterface.showSettingsInterface(false);
            }
        });
        ActionListener addEntButtonListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        };
        ActionListener addMmhButtonListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        };
        ActionListener addGhpButtonListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        };
        addButton.addActionListener(addEntButtonListener);
        addButton1.addActionListener(addMmhButtonListener);
        addButton2.addActionListener(addGhpButtonListener);
        setAsDefaultButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (searchSettingsInterface.getGameSearchSettingsController()
                        .saveDefaultGameSearchSettings(getSelectedGameSearchSettings())) {
                    successTextField.setText("Setting saved as default.");
                    Executors.newSingleThreadScheduledExecutor().schedule(new Runnable() {
                        @Override
                        public void run() {
                            successTextField.setText("");
                        }
                    }, 5, TimeUnit.SECONDS);
                }
            }
        });
        successTextField.setBorder(BorderFactory.createEmptyBorder());
        textField2.setBorder(BorderFactory.createEmptyBorder());
    }

    private GameSearchSettings getSelectedGameSearchSettings() {
        GameSearchSettings gameSearchSettings = new GameSearchSettings();
        gameSearchSettings.setEntGame(entList.getSelectedValue());
        gameSearchSettings.setMmhGame(mmhList.getSelectedValue());
        gameSearchSettings.setGhpGame(ghpList.getSelectedValue());
        return gameSearchSettings;
    }

    public JList<String> getMmhList() {
        return mmhList;
    }

    public JList<String> getGhpList() {
        return ghpList;
    }

    public JList<String> getEntList() {
        return entList;
    }

    public JList<String> getJList(ProviderType providerType) {
        switch (providerType) {
            case ENT:
                return getEntList();
            case MMH:
                return getMmhList();
            case GHP:
                return getGhpList();
            default:
                throw new UnsupportedOperationException("No list for this provider.");
        }
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        GamePanel = new JPanel();
        GamePanel.setLayout(new GridLayoutManager(7, 3, new Insets(0, 0, 0, 0), -1, -1));
        GamePanel.setBackground(new Color(-16449276));
        GamePanel.setForeground(new Color(-16449276));
        GamePanel.setRequestFocusEnabled(true);
        OKButton = new JButton();
        OKButton.setText("OK");
        GamePanel.add(OKButton, new GridConstraints(6, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new BorderLayout(0, 0));
        panel2.setBackground(new Color(-9539217));
        GamePanel.add(panel2, new GridConstraints(0, 1, 2, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        panel2.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black), null));
        final JLabel label1 = new JLabel();
        label1.setBackground(new Color(-9539217));
        label1.setHorizontalAlignment(0);
        label1.setHorizontalTextPosition(0);
        label1.setText("Make Me Host");
        panel2.add(label1, BorderLayout.CENTER);
        addButton1 = new JButton();
        addButton1.setText("Add");
        panel2.add(addButton1, BorderLayout.SOUTH);
        final JPanel panel3 = new JPanel();
        panel3.setLayout(new BorderLayout(0, 0));
        panel3.setBackground(new Color(-9539217));
        GamePanel.add(panel3, new GridConstraints(0, 0, 2, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        panel3.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black), null));
        addButton = new JButton();
        addButton.setText("Add");
        panel3.add(addButton, BorderLayout.SOUTH);
        final JLabel label2 = new JLabel();
        label2.setHorizontalAlignment(0);
        label2.setHorizontalTextPosition(11);
        label2.setText("Enterprise Gaming");
        panel3.add(label2, BorderLayout.CENTER);
        final JPanel panel4 = new JPanel();
        panel4.setLayout(new BorderLayout(0, 0));
        panel4.setBackground(new Color(-9539217));
        GamePanel.add(panel4, new GridConstraints(0, 2, 2, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        panel4.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black), null));
        final JLabel label3 = new JLabel();
        label3.setBackground(new Color(-9539217));
        label3.setHorizontalAlignment(0);
        label3.setText("GhostPlay");
        panel4.add(label3, BorderLayout.CENTER);
        addButton2 = new JButton();
        addButton2.setText("Add");
        panel4.add(addButton2, BorderLayout.SOUTH);
        final JPanel panel5 = new JPanel();
        panel5.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        GamePanel.add(panel5, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, 1, 1, null, new Dimension(200, 65), null, 0, false));
        final JScrollPane scrollPane1 = new JScrollPane();
        scrollPane1.setOpaque(true);
        scrollPane1.setVerticalScrollBarPolicy(20);
        scrollPane1.setVisible(true);
        scrollPane1.setWheelScrollingEnabled(true);
        panel5.add(scrollPane1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        entList = new JList();
        entList.setDragEnabled(false);
        entList.setFocusTraversalPolicyProvider(false);
        final DefaultListModel defaultListModel1 = new DefaultListModel();
        entList.setModel(defaultListModel1);
        entList.setPreferredSize(new Dimension(70, 500));
        entList.setSelectionMode(0);
        entList.setValueIsAdjusting(true);
        entList.setVisible(true);
        entList.setVisibleRowCount(8);
        scrollPane1.setViewportView(entList);
        final JPanel panel6 = new JPanel();
        panel6.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        GamePanel.add(panel6, new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, 1, 1, null, new Dimension(200, 65), null, 0, false));
        final JScrollPane scrollPane2 = new JScrollPane();
        panel6.add(scrollPane2, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        mmhList = new JList();
        mmhList.setPreferredSize(new Dimension(70, 500));
        mmhList.setSelectionMode(0);
        scrollPane2.setViewportView(mmhList);
        final JPanel panel7 = new JPanel();
        panel7.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        GamePanel.add(panel7, new GridConstraints(2, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, 1, 1, null, new Dimension(200, 65), null, 0, false));
        final JScrollPane scrollPane3 = new JScrollPane();
        scrollPane3.setAutoscrolls(false);
        panel7.add(scrollPane3, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        ghpList = new JList();
        ghpList.setPreferredSize(new Dimension(70, 500));
        ghpList.setSelectionMode(0);
        scrollPane3.setViewportView(ghpList);
        saveDefaultSearchSettingButton = new JButton();
        saveDefaultSearchSettingButton.setText("Save to file");
        GamePanel.add(saveDefaultSearchSettingButton, new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        loadSettingButton = new JButton();
        loadSettingButton.setText("Load from file");
        GamePanel.add(loadSettingButton, new GridConstraints(4, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        setAsDefaultButton = new JButton();
        setAsDefaultButton.setText("Set as default");
        GamePanel.add(setAsDefaultButton, new GridConstraints(5, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        successTextField = new JTextField();
        successTextField.setBackground(new Color(-16449276));
        successTextField.setDisabledTextColor(new Color(-12666070));
        successTextField.setEditable(false);
        successTextField.setForeground(new Color(-12666070));
        successTextField.setText("");
        GamePanel.add(successTextField, new GridConstraints(5, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(120, -1), null, 0, false));
        textField2 = new JTextField();
        textField2.setBackground(new Color(-16449276));
        textField2.setEditable(false);
        GamePanel.add(textField2, new GridConstraints(5, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return GamePanel;
    }
}
