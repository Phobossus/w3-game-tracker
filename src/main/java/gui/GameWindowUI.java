package gui;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import controls.UserInterface;
import tracker.providers.ProviderType;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;

import static notifiers.Notifier.soundPlayer;

public class GameWindowUI {
    private final UserInterface userInterface;
    private JFrame mainFrame;
    private JPanel panel1;
    private JButton getNameButton;
    private JButton getModesButton;
    private JTextField Name;
    private JTextField Slots;
    private JCheckBox keepSearchingCheckBox;
    private JTextField loadingText;
    private JLabel Player1;
    private JLabel Player2;
    private JLabel Player3;
    private JLabel Player4;
    private JCheckBox alwaysCopyCheckBox;
    private JCheckBox Mute;
    private JLabel Player5;
    private JLabel Player6;
    private JLabel Player7;
    private JLabel Player10;
    private JLabel Player11;
    private JLabel Player12;
    private JLabel Player8;
    private JLabel Player9;
    private JComboBox ProviderList;
    private JPanel Players;
    private JButton chooseGameButton;
    private List<JLabel> PlayersLabels;
    private Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    private String Modes = "((No game to get modes for))";
    private boolean autoNameCopyOn = true;

    public GameWindowUI(final UserInterface userInterface) {
        this.userInterface = userInterface;
        $$$setupUI$$$();

        this.PlayersLabels = Arrays.asList(Player1, Player2, Player3, Player4, Player5, Player6,
                Player7, Player8, Player9, Player10, Player11, Player12);

        getNameButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                copyToClipboard(Name.getText());
            }
        });
        getModesButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                copyToClipboard(Modes);
            }
        });
        keepSearchingCheckBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (keepSearchingCheckBox.isSelected()) {
                    userInterface.enableSearching();
                }
            }
        });
        alwaysCopyCheckBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (alwaysCopyCheckBox.isSelected()) {
                    autoNameCopyOn = true;
                } else {
                    autoNameCopyOn = false;
                }
            }
        });
        Name.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                if (autoNameCopyOn) {
                    copyToClipboard(Name.getText());
                }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {

            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                if (autoNameCopyOn) {
                    copyToClipboard(Name.getText());
                }
            }
            // implement the methods
        });
        Mute.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (Mute.isSelected()) {
                    soundPlayer.mute();
                } else {
                    soundPlayer.unmute();
                }
            }
        });
        ProviderList.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                userInterface.setSelectedProviderType(getSelectedProviderType(ProviderList.getSelectedItem()));
            }
        });
        chooseGameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                userInterface.openSearchSettingsInterface();
            }
        });
    }

    private ProviderType getSelectedProviderType(Object selectedLabel) {
        switch ((String) selectedLabel) {
            case "ENT":
                return ProviderType.ENT;
            case "MMH":
                return ProviderType.MMH;
            case "GHP":
                return ProviderType.GHP;
            default:
                throw new IllegalStateException();
        }
    }

    private void copyToClipboard(String text) {
        StringSelection stringSelection = new StringSelection(text);
        clipboard.setContents(stringSelection, null);
    }

    public JLabel getPlayer1() {
        return Player1;
    }


    public JLabel getPlayer2() {
        return Player2;
    }


    public JLabel getPlayer3() {
        return Player3;
    }


    public JLabel getPlayer4() {
        return Player4;
    }

    public JPanel getPanel() {
        return panel1;
    }

    public JCheckBox getMute() {
        return Mute;
    }

    public JCheckBox getKeepSearchingCheckBox() {
        return keepSearchingCheckBox;
    }

    public JTextField getSlots() {
        return Slots;
    }

    public String getModes() {
        return Modes;
    }

    public void setModes(String mode) {
        this.Modes = mode;
    }

    public JTextField getName() {
        return Name;
    }

    public JTextField getLoadingText() {
        return loadingText;
    }

    public JButton getGetModesButton() {
        return getModesButton;
    }

    public JCheckBox getAlwaysCopyCheckBox() {
        return alwaysCopyCheckBox;
    }

    public void setMainFrame(JFrame mainFrame) {
        this.mainFrame = mainFrame;
    }

    public JFrame getMainFrame() {
        return mainFrame;
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }

    public List<JLabel> getPlayersLabels() {
        return PlayersLabels;
    }

    public JComboBox getProviderList() {
        return ProviderList;
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        panel1 = new JPanel();
        panel1.setLayout(new GridLayoutManager(5, 5, new Insets(0, 0, 0, 0), -1, -1));
        panel1.setBackground(new Color(-16449276));
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new BorderLayout(0, 0));
        panel1.add(panel2, new GridConstraints(0, 0, 2, 5, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        Name = new JTextField();
        Name.setAlignmentX(0.5f);
        Name.setDisabledTextColor(new Color(-16449276));
        Name.setEnabled(false);
        Name.setFont(new Font("Inconsolata", Name.getFont().getStyle(), Name.getFont().getSize()));
        Name.setForeground(new Color(-1));
        Name.setMargin(new Insets(0, 0, 0, 0));
        Name.setText("And the game for today is ...");
        panel2.add(Name, BorderLayout.NORTH);
        Slots = new JTextField();
        Slots.setDisabledTextColor(new Color(-16449276));
        Slots.setEnabled(false);
        Slots.setForeground(new Color(-1));
        panel2.add(Slots, BorderLayout.SOUTH);
        Players = new JPanel();
        Players.setLayout(new GridLayoutManager(6, 2, new Insets(0, 0, 0, 0), -1, -1));
        panel2.add(Players, BorderLayout.CENTER);
        Player1 = new JLabel();
        Player1.setForeground(new Color(-4582647));
        Player1.setText("Player 1");
        Players.add(Player1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        Player4 = new JLabel();
        Player4.setForeground(new Color(-16449276));
        Player4.setText("Player 4");
        Players.add(Player4, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        Player5 = new JLabel();
        Player5.setForeground(new Color(-16449276));
        Player5.setText("Player 5");
        Players.add(Player5, new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        Player6 = new JLabel();
        Player6.setForeground(new Color(-16449276));
        Player6.setText("Player 6");
        Players.add(Player6, new GridConstraints(5, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        Player7 = new JLabel();
        Player7.setForeground(new Color(-16449276));
        Player7.setText("Player 7");
        Players.add(Player7, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        Player10 = new JLabel();
        Player10.setForeground(new Color(-16449276));
        Player10.setText("Player 10");
        Players.add(Player10, new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        Player11 = new JLabel();
        Player11.setForeground(new Color(-16449276));
        Player11.setText("Player 11");
        Players.add(Player11, new GridConstraints(4, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        Player12 = new JLabel();
        Player12.setForeground(new Color(-16449276));
        Player12.setText("Player 12");
        Players.add(Player12, new GridConstraints(5, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        Player8 = new JLabel();
        Player8.setForeground(new Color(-16449276));
        Player8.setText("Player 8");
        Players.add(Player8, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        Player2 = new JLabel();
        Player2.setForeground(new Color(-16449276));
        Player2.setText("Player 2");
        Players.add(Player2, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        Player9 = new JLabel();
        Player9.setForeground(new Color(-16449276));
        Player9.setText("Player 9");
        Players.add(Player9, new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        Player3 = new JLabel();
        Player3.setForeground(new Color(-16449276));
        Player3.setText("Player 3");
        Players.add(Player3, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        keepSearchingCheckBox = new JCheckBox();
        keepSearchingCheckBox.setBackground(new Color(-16449276));
        keepSearchingCheckBox.setSelected(true);
        keepSearchingCheckBox.setText("Keep Searching");
        panel1.add(keepSearchingCheckBox, new GridConstraints(4, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        loadingText = new JTextField();
        loadingText.setBackground(new Color(-16449276));
        loadingText.setDisabledTextColor(new Color(-1));
        loadingText.setEnabled(false);
        loadingText.setSelectedTextColor(new Color(-16312255));
        loadingText.setText("");
        panel1.add(loadingText, new GridConstraints(4, 2, 1, 3, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        getModesButton = new JButton();
        getModesButton.setText("Get Modes");
        panel1.add(getModesButton, new GridConstraints(2, 4, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        getNameButton = new JButton();
        getNameButton.setText("Get Name");
        panel1.add(getNameButton, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        alwaysCopyCheckBox = new JCheckBox();
        alwaysCopyCheckBox.setBackground(new Color(-16449276));
        alwaysCopyCheckBox.setSelected(true);
        alwaysCopyCheckBox.setText("Always copy ");
        panel1.add(alwaysCopyCheckBox, new GridConstraints(3, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        Mute = new JCheckBox();
        Mute.setBackground(new Color(-16449276));
        Mute.setText("Mute");
        panel1.add(Mute, new GridConstraints(3, 4, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        ProviderList = new JComboBox();
        final DefaultComboBoxModel defaultComboBoxModel1 = new DefaultComboBoxModel();
        defaultComboBoxModel1.addElement("ENT");
        defaultComboBoxModel1.addElement("MMH");
        defaultComboBoxModel1.addElement("GHP");
        ProviderList.setModel(defaultComboBoxModel1);
        ProviderList.setToolTipText("Select game provider");
        panel1.add(ProviderList, new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        chooseGameButton = new JButton();
        chooseGameButton.setText("Choose game");
        panel1.add(chooseGameButton, new GridConstraints(2, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return panel1;
    }
}
