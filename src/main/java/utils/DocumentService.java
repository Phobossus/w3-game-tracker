package utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class DocumentService {

    private static final Logger LOGGER = LogManager.getLogger(DocumentService.class);
    private static final String MOZILLA_USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:5.0) " +
            "Gecko/20100101 Firefox/5.0";

    public Document getDocument(String targetUrl) {
        try {
            return Jsoup.connect(targetUrl)
                    .userAgent(MOZILLA_USER_AGENT)
                    .post();
        } catch (IOException e) {
            LOGGER.error("Error connecting to {}", targetUrl, e);
        }
        return null; // yeah, I've made myself dependent on a Java 1.7 UI framework, r.i.p optionals
    }

}
