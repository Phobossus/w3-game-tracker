package utils;

import javax.swing.*;

public class ErrorDialogs {

    public static void showErrorDialog(String message) {
        JOptionPane.showMessageDialog(new JFrame(), message,
                "Error", JOptionPane.ERROR_MESSAGE);
    }

    public static void showInfoDialog(String message) {
        JOptionPane.showMessageDialog(new JFrame(), message,
                "Info", JOptionPane.INFORMATION_MESSAGE);
    }

    public static void showWarningDialog(String message) {
        JOptionPane.showMessageDialog(new JFrame(), message,
                "Warning", JOptionPane.WARNING_MESSAGE);
    }

}
