package tracker.providers;

import tracker.parsers.MmhFeedParser;
import games.GamesCollection;
import utils.DocumentService;

public class MmhGameStateProvider extends GameStateProvider {
    private static final String GAME_ADDRESS = "http://www.makemehost.com/refresh/divGames-table-partners.php";

    public MmhGameStateProvider(String defaultGameName, GamesCollection gamesCollection) {
        super(new DocumentService(), new MmhFeedParser(), defaultGameName, gamesCollection);
    }

    @Override
    public String getGameProviderAddress() {
        return GAME_ADDRESS;
    }

    protected String getPattern(String gameName) {
        gameName = gameName.replaceAll("\\[", "\\\\[");
        gameName = gameName.replaceAll("\\]", "\\\\]");
        return gameName + " #\\d+ \\d+/\\d+";
    }

}
