package tracker.providers;

public enum ProviderType {
    ENT,
    MMH,
    GHP
}
