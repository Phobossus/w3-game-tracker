package tracker.providers;

import controls.SearchSettingsInterface;
import controls.UserInterface;
import notifiers.Notifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import tracker.state.GameState;
import utils.DocumentService;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public class FeedDownloader implements Runnable {

    private static final Logger LOGGER = LogManager.getLogger(FeedDownloader.class);
    private static final int TIME_AFTER_CALL = 500;
    private final UserInterface userInterface;
    private final SearchSettingsInterface searchSettingsInterface;
    private final List<Notifier> notifiers;
    private final Map<ProviderType, GameStateProvider> gameStateProviders;
    private final DocumentService documentService;

    public FeedDownloader(UserInterface userInterface, SearchSettingsInterface searchSettingsInterface,
                          List<Notifier> registeredNotifiers, Map<ProviderType, GameStateProvider> gameStateProviders,
                          DocumentService documentService) {
        this.userInterface = userInterface;
        this.searchSettingsInterface = searchSettingsInterface;
        this.notifiers = registeredNotifiers;
        this.gameStateProviders = gameStateProviders;
        this.documentService = documentService;
    }

    public void run() {
        while (userInterface.shouldKeepSearching()) {
            ProviderType providerType = userInterface.getSelectedProviderType();
            Optional<GameState> optionalGameState = downloadGameState(providerType);
            if (optionalGameState.isPresent()) {
                GameState gameState = optionalGameState.get();
                userInterface.displayGame(gameState);
                if (gameState.getName() != null && gameState.getSlots() != null) {
                    userInterface.setLoadingMessage(false);
                }
                if (gameState.getRegisteredNotifiers().isEmpty()) {
                    gameState.registerNotifiers(notifiers);
                }
            }
        }
    }

    private Optional<GameState> downloadGameState(ProviderType providerType) {
        GameState gameState = null;
        try {
            GameStateProvider gameStateProvider = gameStateProviders.get(providerType);
            gameStateProvider.setGameToSearch(searchSettingsInterface.getGameToSearch(providerType));
            Thread.sleep(TIME_AFTER_CALL);
            gameState = gameStateProvider
                    .getGameStateData(documentService.getDocument(gameStateProvider.getGameProviderAddress()));
        } catch (InterruptedException e) {
            LOGGER.error("Error downloading the content of the page for provider {}", providerType, e);
        } catch (IllegalArgumentException e) {
            LOGGER.error("Incorrect URL for provider: {}", providerType, e);
        }
        return Optional.ofNullable(gameState);
    }

}
