package tracker.providers;

import tracker.parsers.GhostdeFeedParser;
import games.GamesCollection;
import utils.DocumentService;

public class GhostdeGameStateProvider extends GameStateProvider {
    private static final String GAME_ADDRESS = "https://ghostplay.de/stats/ajax/getLiveGames";

    public GhostdeGameStateProvider(String defaultGameName, GamesCollection gamesCollection) {
        super(new DocumentService(), new GhostdeFeedParser(), defaultGameName, gamesCollection);
    }

    @Override
    public String getGameProviderAddress() {
        return GAME_ADDRESS;
    }

    protected String getPattern(String gameName) {
        gameName = gameName.replaceAll("\\[", "\\\\[");
        gameName = gameName.replaceAll("\\]", "\\\\]");
        return "\\d+/\\d+ " + gameName + " #\\d+.+?\\d*/\\d*";
    }

}
