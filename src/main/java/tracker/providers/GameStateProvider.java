package tracker.providers;

import errors.ProviderInputParseException;
import games.GamesCollection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.nodes.Document;
import tracker.parsers.FeedParser;
import tracker.state.GameState;
import tracker.state.GameStateUpdater;
import utils.DocumentService;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class GameStateProvider {

    private static final Logger LOGGER = LogManager.getLogger(GameStateProvider.class);
    private final DocumentService documentService;
    private final FeedParser feedParser;
    private final GameStateUpdater gameStateUpdater;
    private Pattern pattern;

    public GameStateProvider(DocumentService documentService, FeedParser feedParser, String defaultGameName,
                             GamesCollection gamesCollection) {
        this.documentService = documentService;
        this.feedParser = feedParser;
        this.gameStateUpdater = new GameStateUpdater(new GameState(), gamesCollection);
        this.pattern = Pattern.compile(getPattern(defaultGameName));
    }

    protected abstract String getGameProviderAddress();

    protected abstract String getPattern(String gameName);

    public GameState getGameStateData(Document gamesHtmlDocument) {
        Matcher matcher = pattern.matcher(gamesHtmlDocument.text());
        boolean foundGame = matcher.find();
        if (foundGame) {
            try {
                gameStateUpdater.updateState(feedParser.parseGameStateFeed(matcher.group()));
            } catch (ProviderInputParseException e) {
                LOGGER.error("Error while parsing game state feed for " + feedParser.getClass() +
                        ". Data format might have changed.", e);
                foundGame = false;
            }
        }
        return foundGame ? gameStateUpdater.getLastUpdate() : gameStateUpdater.getDummyGame();
    }

    public void setGameToSearch(String gameName) {
        String expression = getPattern(gameName);
        if (!pattern.pattern().equals(expression)) {
            pattern = Pattern.compile(expression);
        }
    }

    public static GameStateProvider createGameStateProvider(ProviderType providerType,
                                                            String defaultGameName,
                                                            GamesCollection gamesCollection) {
        switch (providerType) {
            case ENT:
                return new EntGameStateProvider(defaultGameName, gamesCollection);
            case MMH:
                return new MmhGameStateProvider(defaultGameName, gamesCollection);
            case GHP:
                return new GhostdeGameStateProvider(defaultGameName, gamesCollection);
            default:
                throw new IllegalArgumentException("Provider not supported.");
        }
    }

}

