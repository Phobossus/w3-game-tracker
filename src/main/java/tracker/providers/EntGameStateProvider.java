package tracker.providers;

import tracker.parsers.EntFeedParser;
import games.GamesCollection;
import utils.DocumentService;

public class EntGameStateProvider extends GameStateProvider {

    private static final String GAME_ADDRESS = "https://entgaming.net/forum/games_fast.php";

    public EntGameStateProvider(String defaultGameName, GamesCollection gamesCollection) {
        super(new DocumentService(), new EntFeedParser(), defaultGameName, gamesCollection);
    }

    @Override
    public String getGameProviderAddress() {
        return GAME_ADDRESS;
    }

    protected String getPattern(String gameName) {
        gameName = gameName.replaceAll("\\[", "\\\\[");
        gameName = gameName.replaceAll("\\]", "\\\\]");
        return "\\d+(\\|\\d+){4}\\|" + gameName + " #\\d*";
    }
}
