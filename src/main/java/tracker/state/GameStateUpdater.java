package tracker.state;

import games.GamesCollection;
import tracker.model.ParsedGameStateFeed;

import java.util.Collections;
import java.util.List;

public class GameStateUpdater {
    private static final String ENT_SLOTS_ADDRESS = "https://entgaming.net/forum/slots_fast.php?id=";
    private final GamesCollection gamesCollection;
    private final GameState dummyGame;
    private GameState lastUpdate;

    public GameStateUpdater(GameState lastUpdate, GamesCollection gamesCollection) {
        this.gamesCollection = gamesCollection;
        this.dummyGame = buildDummyGame();
        this.lastUpdate = lastUpdate;
    }

    public GameState getLastUpdate() {
        return lastUpdate;
    }

    public void updateState(ParsedGameStateFeed parsedGameStateFeed) {
        String id = parsedGameStateFeed.getId();
        String name = parsedGameStateFeed.getName();
        String slots = parsedGameStateFeed.getSlots();
        List<String> players = lastUpdate.getPlayers();

        if (isNewGame(id)) {
            lastUpdate.setId(id);
            lastUpdate.setCurrentSlotsAddress(ENT_SLOTS_ADDRESS + id);
            lastUpdate.setModes(gamesCollection.getGameModesMap().get(parsedGameStateFeed.getRawName()));
        }
        if (!name.equals(lastUpdate.getName())) {
            lastUpdate.setName(name);
        }
        if (!slots.equals(lastUpdate.getSlots())) {
            lastUpdate.setSlots(slots);
        }
        if (!parsedGameStateFeed.getPlayerLobby().equals(players)) {
            lastUpdate.setPlayers(parsedGameStateFeed.getPlayerLobby());
        }
    }

    public GameState getDummyGame() {
        return dummyGame;
    }

    private boolean isNewGame(String id) {
        String lastId = lastUpdate.getId();
        return lastId == null || !id.equals(lastId);
    }

    private GameState buildDummyGame() {
        GameState dummyGame = new GameState();
        dummyGame.setName("<<< NO GAME FOUND >>>");
        dummyGame.setSlots("The bot is offline or it has changed the game name.");
        dummyGame.setPlayers(Collections.emptyList());
        dummyGame.setId("dummyId");
        return dummyGame;
    }
}
