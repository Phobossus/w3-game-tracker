package tracker.state;

import notifiers.FullGameNotifier;
import notifiers.ObservableGameState;
import notifiers.Observer;
import notifiers.PlayerJoinedNotifier;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class GameState extends ObservableGameState {

    private List<String> players = new ArrayList<>();
    private String slots;
    private String currentSlotsAddress;
    private String name;
    private String id;
    private String modes;

    public List<String> getPlayers() {
        return players;
    }

    public String getSlots() {
        return slots;
    }

    public String getCurrentSlotsAddress() {
        return currentSlotsAddress;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getModes() {
        return modes;
    }

    public void setSlots(String slots) {
        this.slots = slots;
        sendFullGameNotification();
    }

    public void setPlayers(List<String> players) {
        this.players = players;
        sendPlayerJoinedNotification();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCurrentSlotsAddress(String currentSlotsAddress) {
        this.currentSlotsAddress = currentSlotsAddress;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setModes(String modes) {
        this.modes = modes;
    }

    private void sendPlayerJoinedNotification() {
        getExecutorService().execute(new Runnable() {
            @Override
            public void run() {
                Optional<Observer> optionalObserver = GameState.this.getSpecificObserver(PlayerJoinedNotifier.class);
                if (optionalObserver.isPresent()) {
                    Observer observer = optionalObserver.get();
                    observer.onUpdate(players);
                }
            }
        });
    }

    private void sendFullGameNotification() {
        if (isFull()) {
            getExecutorService().execute(new Runnable() {
                @Override
                public void run() {
                    Optional<Observer> observer = GameState.this.getSpecificObserver(FullGameNotifier.class);
                    if (observer.isPresent()) {
                        FullGameNotifier obs = (FullGameNotifier) observer.get();
                        obs.onUpdate(players);
                    }
                }
            });
        }
    }

    private boolean isFull() {
        String[] count = slots.split("/");
        return count.length >= 2 && count[0].equals(count[1]);
    }

}
