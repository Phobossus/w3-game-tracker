package tracker.model;

import lombok.ToString;

import java.util.List;

@ToString
public class ParsedGameStateFeed {
    private String id;
    private String name;
    private String slots;
    private String rawName;
    private List<String> playerLobby;

    public String getRawName() {
        return rawName;
    }

    public void setRawName(String rawName) {
        this.rawName = rawName;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSlots(String slots) {
        this.slots = slots;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSlots() {
        return slots;
    }

    public List<String> getPlayerLobby() {
        return playerLobby;
    }

    public void setPlayerLobby(List<String> playerLobby) {
        this.playerLobby = playerLobby;
    }
}


