package tracker.parsers;

import errors.ProviderInputParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import tracker.model.ParsedGameStateFeed;
import utils.DocumentService;

import java.util.ArrayList;
import java.util.List;

public class EntFeedParser implements FeedParser<String> {

    private static final Logger LOGGER = LogManager.getLogger(EntFeedParser.class);
    private static final String GAME_SEPARATOR = ("\\|");
    private static final String ENT_SLOTS_ADDRESS = "https://entgaming.net/forum/slots_fast.php?id=";
    private final DocumentService documentService = new DocumentService();

    @Override
    public ParsedGameStateFeed parseGameStateFeed(String line) throws ProviderInputParseException {
        try {
            String[] elements = line.split(GAME_SEPARATOR, 6);
            ParsedGameStateFeed parsedGameStateFeed = new ParsedGameStateFeed();
            parsedGameStateFeed.setId(elements[0]);
            parsedGameStateFeed.setRawName(elements[5].split("\\s#\\d+")[0]);
            parsedGameStateFeed.setName(elements[5]);
            parsedGameStateFeed.setSlots(elements[2] + "/" + elements[3]);
            parsedGameStateFeed.setPlayerLobby(parsePlayerLobby(parsedGameStateFeed.getId()));
            return parsedGameStateFeed;
        } catch (RuntimeException e) {
            throw new ProviderInputParseException(e);
        }
    }

    private List<String> parsePlayerLobby(String gameId) throws ProviderInputParseException {
        Document playersDocument = documentService.getDocument(ENT_SLOTS_ADDRESS + gameId);
        try {
            List<String> playersInGame = new ArrayList<>();
            Element table = playersDocument.select("table").first();
            Elements rows = table.select("tr");
            for (Element column : rows) {
                Elements playerSlot = column.select("td[class=slot]");
                if (!playerSlot.isEmpty()) {
                    playersInGame.add(playerSlot.first().text());
                }
            }
            return playersInGame;
        } catch (RuntimeException e) {
            throw new ProviderInputParseException(e);
        }
    }

//    # In case they ever bring this feed back #
    //private static final String PLAYER_SEPARATOR = "Username Realm Ping";
//    public List<String> parsePlayerLobby(String line) {
//        return Arrays.asList(line.split(PLAYER_SEPARATOR)[1].split("\\s", -1));
//    }
}
