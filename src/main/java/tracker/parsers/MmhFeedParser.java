package tracker.parsers;

import errors.ProviderInputParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import tracker.model.ParsedGameStateFeed;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class MmhFeedParser implements FeedParser<String> {

    private static final Logger LOGGER = LogManager.getLogger(MmhFeedParser.class);
    private String lastUpdateSlots;

    @Override
    public ParsedGameStateFeed parseGameStateFeed(String line) throws ProviderInputParseException {
        try {
            String[] elements = line.split("\\s", -1);
            ParsedGameStateFeed parsedGameStateFeed = new ParsedGameStateFeed();
            if (elements.length > 1) {
                StringBuilder gameName = new StringBuilder();
                int i;
                for (i = 0; i < elements.length && !elements[i].matches("#\\d+"); i++) { // "\\d+/\\d+"
                    gameName.append(elements[i]).append(" ");
                }
                String slots = elements[i + 1];

                parsedGameStateFeed.setRawName(gameName.toString().trim());
                parsedGameStateFeed.setName(gameName.append(elements[i]).append(" ").toString().trim()); // gameName.toString
                parsedGameStateFeed.setId(elements[i]); // i-1
                parsedGameStateFeed.setSlots(slots); // i
                parsedGameStateFeed.setPlayerLobby(parsePlayerLobby(slots));
            }
            return parsedGameStateFeed;
        } catch (RuntimeException e) {
            throw new ProviderInputParseException(e);
        }
    }

    private List<String> parsePlayerLobby(String slots) {
        // TODO: prevent dummyLobby being overwritten on next Iteration by empty list
        List<String> dummyLobby = null;
        if (lastUpdateSlots == null || !lastUpdateSlots.equals(slots)) {
            lastUpdateSlots = slots;
            dummyLobby = new LinkedList<>();
            int playersInGame = Integer.parseInt(slots.split("/")[0]);
            dummyLobby.add("This provider only shows taken/closed slots in lobby.");
            for (int i = 0; i < playersInGame; i++) {
                dummyLobby.add("P" + i);
            }
        }
        return dummyLobby == null ? Collections.emptyList() : dummyLobby;
    }
}