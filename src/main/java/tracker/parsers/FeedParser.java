package tracker.parsers;

import errors.ProviderInputParseException;
import tracker.model.ParsedGameStateFeed;

public interface FeedParser<G> {

    ParsedGameStateFeed parseGameStateFeed(G input) throws ProviderInputParseException;

}
