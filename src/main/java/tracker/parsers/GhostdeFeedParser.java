package tracker.parsers;

import errors.ProviderInputParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import tracker.model.ParsedGameStateFeed;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

// TODO: na bazie maksymalnej liczby slotow powinny byc ustawiane "Empty" a nie "-"

public class GhostdeFeedParser implements FeedParser<String> {

    private static final Logger LOGGER = LogManager.getLogger(GhostdeFeedParser.class);
    private static final String GAME_SEPARATOR = "\\s";
    private static final String LOBBY_SEPARATOR = "#\\d*\\s";

    @Override
    public ParsedGameStateFeed parseGameStateFeed(String line) throws ProviderInputParseException {
        try {
            line = line.substring(0, line.length() - 3);
            String[] elements = line.split(GAME_SEPARATOR, -1);
            ParsedGameStateFeed parsedGameStateFeed = new ParsedGameStateFeed();
            if (elements.length > 1) {
                StringBuilder rawGameName = new StringBuilder();
                int i;
                for (i = 1; i < elements.length && !elements[i].matches("#\\d+"); i++) {
                    rawGameName.append(elements[i]).append(" ");
                }
                parsedGameStateFeed.setSlots(elements[0]);
                parsedGameStateFeed.setId(elements[i]);
                parsedGameStateFeed.setRawName(rawGameName.toString().trim());
                parsedGameStateFeed.setName(rawGameName.append(elements[i]).toString());
            }
            parsedGameStateFeed.setPlayerLobby(parsePlayerLobby(line));
            return parsedGameStateFeed;
        } catch (RuntimeException e) {
            throw new ProviderInputParseException(e);
        }
    }

    private List<String> parsePlayerLobby(String line) {
        String players[] = line.split(LOBBY_SEPARATOR);
        boolean anyPlayersInGame = players.length > 1;
        if (anyPlayersInGame) {
            players = players[1].split(GAME_SEPARATOR);
        }
        return anyPlayersInGame ? Arrays.asList(players) : Collections.emptyList();
    }

}
