# **Warcraft 3 Game Tracker**

![Scheme](images/gametracker-main.PNG)  

Simple java desktop application that makes joining Warcraft 3 bot hosted games easier. Supported bots:  
- ENT (https://entgaming.net/forum/games.php)  
- Ghostplay (https://ghostplay.de/stats/)  
- MakeMeHost (http://www.makemehost.com/games.php)  
It integrates with game lists that those hosts provide.  

Game Tracker automates some of the manual, time consuming actions that you probably do every so often while joining 
a rehosted game or waiting in the lobby for it to get ready. So what have I done to make your life better? Let's see
what Game Tracker offers!  

-----------------------

## **Download**
https://bitbucket.org/Phobossus/w3-game-tracker/downloads/W3-GameTracker.rar  
Installing: Unzip it wherever you want.  
Note: You should have Java >= 1.7 installed in your operating system. 
(https://www.java.com/en/download/help/download_options.xml)  


-----------------------

## **Features**  

* **Watch game lobby anywhere on your desktop anytime you want!**  
Turn Game Tracker on. Its size is so small you can put it in the corner of your screen or anywhere you please. It's 
also set to always stay on top of any other application.  

* **Get sound notifications when the lobby is full!**  
Want to clean your room or read some book while waiting? No problem. Game Tracker **will definitely** let you know
when the game you're watching fills up 100%. Your ears and `horn.wav` don't get along? Replace it with another .wav
file of your choosing.

* **Get sound notifications when a player from your custom Observed.txt list has joined!**  
Are you waiting for a friend to finish his game so the two of you can play together? Add his nickname to 
`Observed.txt`
file. More friends? Oh, some enemies you want to fight again to get your revenge? Add them all! Game Tracker will
notify you when they join (sound file is also customizable)  

* **Auto copy of selected game's name!**  
On by default. Helpful especially for rejoining remakes, i.e if the game you track is Castle Fight 2v2, you'll always have
the latest hosted game name (`Castle Fight 2v2 #376`, then #377 etc) in your clipboard - simply paste it in Warcraft 
instead of alt tabbing and checking the sites.  
Manual `Get Name` button also exists for those who want to control their clipboard.

* **Auto copy game modes after the game you're in has started!**  
Do you happen to be a host quite often? Don't want to memorize modes or tired of endlessly rewriting them? Check
`Games.txt`, where you can specify the text that is copied to your clipboard after the game fills up! It may be modes,
or it may be anything you want.  
Manual `Get Modes` button also exists for those who want to control their clipboard.

* **Create different tracking configurations!**  
You a Castle Fight player? Be ready to play any hosted Castle Fight there is at the time! Check Castle Fight game
names for each bot and add them to Games.txt under their respective provider names. Restart GameTracker and select
a configuration you're interested in (example picture included), then `save to file`. From then on you can load
it each time you feel like playing Castle Fight and easily switch between tracking different bot games by simply 
choosing `ENT`, `GHP` or `MMH` from the list of providers whenever you wish to check how the other game lobby is 
doing.  
Want GameTracker to load this configuration by default? Click `Set as default` to make that happen.  

![Scheme](images/GameTracker-settings.PNG)  

**DISCLAIMER:** Some of the features are highly dependent on specific bot sites and the information that they provide.
As an example, `MakeMeHost` does not show what players have joined the game lobby, so sending notifications about
the people that you observe is not possible. Nevertheless, the fundamental feature of monitoring the number of 
players itself and notifying when the game becomes full works for all bots.`   

-----------------------

## **Some history** 
The Castle Fight references are not accidental - this application was originally created to help me and my ally
play Castle Fight 2v2 and minimize the boring activities in-between. Basically, we didn't want to:  

* search for a new game name each time we finished our last  
* write the modes from scratch on every join  
* open 3 dedicated web sites with game lists for 3 different bots and regularly check them  
* alt-tab to tell each other that we've finished our game and that we're about to start another  
* manually observe game lobbies on the sites looking for when certain players who we wanted to play with appear    

**W3 Game Tracker** started off as **CF Helper** and eventually grew up to feature any kind of game that matches the
two following criteria:  
1. Is hosted by `ENT`, `GHP` or `MMh`  
2. Follows the naming pattern `gamename#gamenumber`  
i.e `[BK] Legion TD #431` is a valid game that Game Tracker can track.  

-----------------------

## **For people with technical background**
This is an Open-source software - you can improve it or fix any found bugs by cloning the repository and submitting
pull requests, which I will review and accept or deny ;)  
### **Building project**   
You will need:  

* Java 1.7 (I have used a GUI builder which sadly is not compatible with Java 8, but I realized it too late)   
* Maven (https://maven.apache.org/download.cgi)
* Idea UI Designer maven plugin

To build the project simply run `mvn clean install` in the project directory.  

### **Project overwiew**  
The project started off small and was not planned to be extended or reach great sizes - this is why there are no 
heavy frameworks used (i.e there is no Spring to handle dependency injection in order to minimize the size of 
final .jar file). For the same reason, there are no unit tests as I was rushing to deliver it as soon as possible.  
Having encountered bugs and unexpected turn of events (providers changing the data format they serve from time to 
time) I regret not adding different scenario unit tests or e2e tests for the parsers - if there happens to be a 
problem, change of data format is where it usually is located. Perhaps one day I will repair the mistakes of the past me.

### **Short descriptions of the key components**  

**ApplicationInitializer**  
This is where all settings and all configuration files are loaded and initialized, including:

* notifiers
* collection of games written in Games.txt
* saved search settings and default search settings 
* game providers and parsers 

**FeedDownloader**  
The main thread that connects to external sites and downloads their content. It makes a call once per ~0.5 seconds.
Downloaded documents are then passed to the appropriate GameStateProviders.  

**GameStateProvider**  
One per each of the hosting bots. It determines whether the pattern of game being tracked matches any line of the
downloaded game list. In case it is, it delegates the process of filtering and parsing the data to the 
respective FeedParser.  

**FeedParser**  
One for each of the hosting bots. It extracts the required data, which is next passed to the GameStateUpdater.
Note: Since ENT, Ghostplay and MakeMeHost all differ in what information about the game and lobbies they display,
some parsers are forced to replace missing data with dummy text. More on that later. 

**GameStateUpdater**  
A component that updates the GameState, filling it with all the data that the parser was able to extract.  

**GameState**  
Basic unit that GameTracker uses to display the information on its interface. It contains all available data 
about the state of game being tracked, such as: name, players in lobbies (where applies) or slots filled. 

**Resources**  
Files in resources are copied to the outcome directory to allow the player to modify them. This includes 
`config.properties`, which is a file that allows to customize the application's behaviour. 

-----------------------

## **Contribution guidelines**  
Download the repository, make changes and submit a pull request. You should also describe what purpose the changes
serve. If you are unsure whether the idea would be accepted or not, you can always consult me before you start
working on it. 

-----------------------

## **Contact** 
Please contact me via email: `aleksander.gul113@gmail.com.` I will happily answer any of your questions or suggestions.  
